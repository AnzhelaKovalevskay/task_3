package com.example.lika

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.net.HttpURLConnection
import java.net.URL

fun main() = runBlocking {
    val args: Array<String> = arrayOf(
        "https://www.google.com",
        "https://www.facebook.com",
        "https://www.github.com",
        "https://www.twitter.com",
        "https://www.instagram.com",
        "https://funpay.com/",
        "https://l2central.info/main/",
        "https://throneandliberty.fun/",
        "https://tarisglobal.com/index.html",
        "https://dzen.ru/?yredirect=true"
    )

    val job = async {
        for (index in args.indices) {
            val url = args[index]
            println(
                "Сайт " + url + if (chekWebsite(url)) " доступен" else " недоступен"
            )
        }
    }
    job.await()
    println("Zadacha stop")
}

suspend fun chekWebsite(url: String): Boolean {
    return try {
        val connection = URL(url).openConnection() as HttpURLConnection
        connection.requestMethod = "HEAD"
        connection.connectTimeout = 5000
        connection.readTimeout = 5000
        connection.responseCode == HttpURLConnection.HTTP_OK
    } catch (e: Exception) {
        false
    }
}
